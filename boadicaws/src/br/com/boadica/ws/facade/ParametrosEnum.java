/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.boadica.ws.facade;

/**
 *
 * @author Daniel Monteiro
 */
public enum ParametrosEnum {

    PAGINA("pag"), CODIGO_LOJA("cl"), ASSINATURA_MD5("api_sig");

    private final String param;

    ParametrosEnum(String param){
        this.param = param;
    }

    public String getValue(){
        return param;
    }

    public String toString(){
        return param;
    }
}
