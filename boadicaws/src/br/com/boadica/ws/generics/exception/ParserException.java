/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.boadica.ws.generics.exception;

import javax.xml.bind.JAXBException;

/**
 *
 * @author Daniel Monteiro
 */
public class ParserException extends Exception {

    public ParserException(String string, JAXBException ex) {
       super(string,ex);
    }

}
