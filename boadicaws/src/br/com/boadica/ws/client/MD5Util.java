/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.boadica.ws.client;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Daniel Monteiro
 */
public class MD5Util {

    /* CODIGO HASH*/
    private static final String MD5 = "MD5";
    
    /**
     * Faz o calculo hash MD5.
     * 
     * @param frase
     * @return {@link byte}
     * @throws NoSuchAlgorithmException
     */
    public static byte[] calculateHash(String frase) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance(MD5);
        md.update(frase.getBytes());
        byte[] hashMd5 = md.digest();
        return hashMd5;
    }

    /**
     * Converte o array de bytes Hash em Hexadecimal para String.
     *
     * @param bytes
     * @return
     */
    public static String stringHexa(byte[] bytes) {
       StringBuilder s = new StringBuilder();
       for (int i = 0; i < bytes.length; i++) {
           int parteAlta = ((bytes[i] >> 4) & 0xf) << 4;
           int parteBaixa = bytes[i] & 0xf;
           if (parteAlta == 0) s.append('0');
           s.append(Integer.toHexString(parteAlta | parteBaixa));
       }
       return s.toString();
    }

    /**
     * Calcula o Hash e retorna a anotação em String.
     * @param frase
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String getHash(String frase) throws NoSuchAlgorithmException{
        return stringHexa(calculateHash(frase));
    }


}
