/**
 * TopaMenu.java
 *
 * Created on Sep 21, 2010 12:02:13 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;
import core.MenuLabel;
import core.SubMenu;
import core.TitleLabel;
import core.BoadicaData;
import core.Category;
import core.PaneLabel;
//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import core.TopBarRequester;
	
/**
 *
 * @author peter
 */
public final class BoadicaMenu extends UpdatableGroup implements Constants, KeyListener, ScreenListener, MenuListener//, TopBarRequester
		//#if TOUCH == "true"
			, PointerListener
		//#endif
{
	private final SubMenu menu;

	private final Category[] categories;

	private Category parent;

	private static Category currentCategory;

//	public boolean TopBarIsRequested() {
//		return true;
//	}

	public static final BoadicaMenu createCategoriesMenu() throws Exception {
		//#if DEBUG == "true"
			System.out.println( "MENU DE CATEGORIAS -> PARENT: " + currentCategory );
		//#endif
		Category[] children = currentCategory == null ? null : currentCategory.getChildren();

		final int backText;

		if ( children == null ) {
			children = BoadicaData.getRootCategories();
			backText = TEXT_EXIT;
		} else {
			backText = TEXT_BACK;
		}

		final String[] childrenNames = new String[ children.length ];

		for ( short i = 0; i < children.length; ++i ) {
			childrenNames[ i ] = children[ i ].getName();
		}

		//#if DEBUG == "true"
			System.out.println( "ENTRADAS: " );
			for ( short i = 0; i < children.length; ++i ) {
				System.out.println( children[ i ] );
			}
			System.out.println();
		//#endif

		final BoadicaMenu m = new BoadicaMenu( SCREEN_CATEGORIES, childrenNames, currentCategory == null ? "" : currentCategory.getName(), currentCategory, children, null );

		return m;
	}


	public BoadicaMenu( int screen, int[] entries, int title, int[] icons ) throws Exception {
		this( screen, getEntries( entries ), title >= 0 ? GameMIDlet.getText( title ) : null, null, null, icons );
	}


	public BoadicaMenu( int screen, String[] entries, int title, int[] icons ) throws Exception {
		this( screen, entries, title >= 0 ? GameMIDlet.getText( title ) : null, null, null, icons );
	}


	public BoadicaMenu( int screen, int[] entries, int title ) throws Exception {
		this( screen, entries, title >= 0 ? GameMIDlet.getText( title ) : null );
	}
	

	public BoadicaMenu( int screen, int[] entries, String titleText ) throws Exception {
		this( screen, getEntries( entries ), titleText );
	}


	public BoadicaMenu( int screen, String[] entries, String titleText ) throws Exception {
		this( screen, entries, titleText, null, null, null );
	}

	
	public BoadicaMenu( int screen, String[] entries, String titleText, Category parent, Category[] categories, int[] icons ) throws Exception {
		super( 10 );

		this.categories = categories;
		this.parent = parent;

		final Drawable[] menuEntries = new Drawable[ entries.length ];
		if ( categories == null ) {
			for ( byte i = 0; i < menuEntries.length; ++i )
				menuEntries[i] = new PaneLabel( entries[ i ], icons[ i ] );
		} else {
			for ( byte i = 0; i < menuEntries.length; ++i )
				menuEntries[i] = new MenuLabel( entries[ i ], parent == null ? i : -1 );
		}

		menu = new SubMenu( this, parent == null ? screen : parent.getId(), menuEntries, MENU_SPACING, 0, entries.length - 1, null );
		menu.setBoadicaMenu( this );
		insertDrawable( menu );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	private static final String[] getEntries( int[] entriesIds ) {
		final String[] ret = new String[ entriesIds.length ];
		
		for ( short i = 0; i < ret.length; ++i ) {
			ret[ i ] = GameMIDlet.getText( entriesIds[ i ] );
		}

		return ret;
	}


	public final SubMenu getMenu() {
		return menu;
	}
	

    public final Drawable getMenuDrawable(int index) {
        return menu.getDrawable(index);
    }


	public final void setCurrentIndex( int index ) {
		menu.setCurrentIndex( index );
	}

	
	public final void keyPressed( int key ) {
		menu.keyPressed( key );
	}


	public final void keyReleased( int key ) {
		menu.keyReleased( key );
	}


	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public final Category getCategoryAt( int index ) {
		try {
			return categories[ index ];
		} catch ( ArrayIndexOutOfBoundsException e ) {
			AppMIDlet.log( e, "a13" );
			return parent;
		}
	}


	public static Category getCurrentCategory() {
		return currentCategory;
	}


	public static void setCurrentCategory( Category current ) {
		BoadicaMenu.currentCategory = current;
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		menu.setSize( width, height );
//		if ( menu.getHeight() < getHeight() ) {
//			menu.setPosition( ( getWidth() - menu.getWidth() ) >> 1, titleHeight + ((( getWidth() - menu.getHeight() ) >> 1) ) );
//        } else {
			menu.setPosition( ( getWidth() - menu.getWidth() ) >> 1, 0 );
//		}

		MenuLabel.setParentY( getPosY() + menu.getPosY() ); // TODO levar em consideração a posição do tabbedpane
	}


	public final void setPosition( int x, int y ) {
		super.setPosition( x, y );

		MenuLabel.setParentY( getPosY() + menu.getPosY() );
	}
	

	public final void onChoose( Menu menu, int id, int index ) {
		if ( categories == null ) {
			( ( GameMIDlet ) GameMIDlet.getInstance() ).onChoose( menu, id, index );
		} else {
			if ( index == -1 ) {
				back( id );
			} else {
				final Category category = getCategoryAt( index );
				setCurrentCategory( category );
				GameMIDlet.setMoveRight( true );

				if ( category.getId() == -1 ) {
					GameMIDlet.setScreen( SCREEN_LOAD_CATEGORIES_INFO );
				} else if ( category.hasChildren() ) {
					GameMIDlet.setScreen( SCREEN_CATEGORIES );
				} else {
					GameMIDlet.setScreen( SCREEN_LOAD_OFFERS );
				}
			}
		}
	}


	public final void back( int id ) {
		if ( parent == null ) {
			setCurrentCategory( null );
			( ( GameMIDlet ) GameMIDlet.getInstance() ).onChoose( menu, id, ENTRY_CATEGORIES_BACK );
		} else {
			setCurrentCategory( BoadicaData.getCategory( parent.getParentId() ) );
			GameMIDlet.setMoveRight( false );
			GameMIDlet.setScreen( SCREEN_CATEGORIES );
		}
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
		( ( GameMIDlet ) GameMIDlet.getInstance() ).onItemChanged( menu, id, index );
	}


	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
			menu.onPointerDragged( x, y );
		}


		public final void onPointerPressed( int x, int y ) {
			menu.onPointerPressed( x, y );
		}


		public final void onPointerReleased( int x, int y ) {
			menu.onPointerReleased( x, y );
		}

	//#endif

}
