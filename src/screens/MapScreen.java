/**
 * MapScreen.java
 *
 * Created on Feb 6, 2011 2:59:51 PM
 *
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.Constants;
import javax.microedition.lcdui.Image;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author peter
 */
public final class MapScreen extends UpdatableGroup implements Constants, KeyListener
//#if TOUCH == "true"
, PointerListener
//#endif

{

	private static DrawableImage MAP;

	private final DrawableImage map;

	//#if TOUCH == "true"
		private final Point lastPointerPos = new Point();
	//#endif

	private final MUV scrollSpeedX = new MUV();
	private final MUV scrollSpeedY = new MUV();

	private int speed;

	private final int backScreen;

	
	public MapScreen( int backScreen ) {
		super( 1 );

		this.backScreen = backScreen;

		map = new DrawableImage( MAP );
		insertDrawable( map );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		map.setPosition( ( getWidth() - map.getWidth() ) >> 1, ( getHeight() - map.getHeight() ) >> 1 );
		checkLimits();
	}

	
	public static final void setMap( Image map ) {
		MapScreen.MAP = map == null ? null : new DrawableImage( map );
	}


	private final void checkLimits() {
		if ( map != null ) {
			final int LIMIT_LEFT = Math.min( getWidth() - map.getWidth(), 0 );
			final int LIMIT_RIGHT = Math.max( getWidth() - map.getWidth(), 0 );
			final int LIMIT_UP = Math.max( getHeight() - map.getHeight(), 0 );
			final int LIMIT_DOWN = Math.min( getHeight() - map.getHeight(), 0 );
			
			map.setPosition( NanoMath.clamp( map.getPosX(), LIMIT_LEFT, LIMIT_RIGHT ),
							 NanoMath.clamp( map.getPosY(), LIMIT_DOWN, LIMIT_UP ) );
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		speed = Math.max( width, height ) >> 1;

		checkLimits();
	}


	public final void update( int delta ) {
		super.update( delta );

		map.move( scrollSpeedX.updateInt( delta ), scrollSpeedY.updateInt( delta ) );
		checkLimits();
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_RIGHT:
//			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_FIRE:
			case ScreenManager.KEY_NUM5:
				GameMIDlet.setMoveRight( false );
				setMap( null );
				GameMIDlet.setScreen( backScreen );
			break;

			case ScreenManager.KEY_LEFT:
			case ScreenManager.KEY_NUM4:
				scrollSpeedX.setSpeed( speed );
			break;

			case ScreenManager.KEY_RIGHT:
			case ScreenManager.KEY_NUM6:
				scrollSpeedX.setSpeed( -speed );
			break;

			case ScreenManager.KEY_UP:
			case ScreenManager.KEY_NUM2:
				scrollSpeedY.setSpeed( speed );
			break;

			case ScreenManager.KEY_DOWN:
			case ScreenManager.KEY_NUM8:
				scrollSpeedY.setSpeed( -speed );
			break;
		}
	}


	public final void keyReleased( int key ) {
		scrollSpeedX.setSpeed( 0 );
		scrollSpeedY.setSpeed( 0 );
	}


	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
			map.move( x - lastPointerPos.x, y - lastPointerPos.y );
			lastPointerPos.set( x, y );
			checkLimits();
		}


		public final void onPointerPressed( int x, int y ) {
			lastPointerPos.set( x, y );
		}


		public final void onPointerReleased( int x, int y ) {
		}
	//#endif
}
