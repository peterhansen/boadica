/**
 * SplashEndemol.java
 *
 * Created on Aug 30, 2010 2:41:00 PM
 *
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.PaletteChanger;
import core.Constants;
import javax.microedition.lcdui.Image;

/**
 *
 * @author peter
 */
public final class SplashNano extends UpdatableGroup implements Constants, ScreenListener {

	private final DrawableImage logo;

	private final RichLabel label;

	private final int COLOR_FONT = 0x0199cb9;

	private static final short TIME_LOGO = 1800;

	private short accTime;


	public SplashNano() throws Exception {
		super( 8 );
		
		logo = new DrawableImage( PATH_SPLASH + "nanostudio.png" );
		logo.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( logo );

		final Image fontImage = new PaletteChanger( PATH_SPLASH + "font_credits.png" ).createImage( 0xffffff, COLOR_FONT );
		final ImageFont font = ImageFont.createMultiSpacedFont( fontImage, PATH_SPLASH + "font_credits.bin" );
		font.setCharExtraOffset( 1 );

		label = new RichLabel( font, TEXT_SPLASH_NANO );
		insertDrawable( label );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public final void update( int delta ) {
		super.update( delta );

		accTime += delta;
		//#if DEBUG == "true"
			accTime += 300;
		//#endif
			
		if ( accTime >= TIME_LOGO )
			GameMIDlet.setScreen( SCREEN_SPLASH_BOADICA );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		logo.setRefPixelPosition( width >> 1, height >> 1 );

		label.setSize( width, height );
		label.setPosition( 0, height - label.getTextTotalHeight() - 2 );
	}

}
