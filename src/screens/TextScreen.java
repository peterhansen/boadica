/**
 * TextScreen.java
 *
 * Created on Sep 21, 2010 5:26:46 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import core.MenuLabel;
import core.TitleLabel;

//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author peter
 */
public final class TextScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener
	//#if TOUCH == "true"
		, PointerListener
	//#endif
{

	private final BasicTextScreen text;

	public final BasicTextScreen getBaseTextScreen(){
		return text;
	}

	public TextScreen( int backIndex, String t, String title, boolean autoScroll, Drawable[] specialChars ) throws Exception {
		super( 5 );

		text = new BasicTextScreen( backIndex, GameMIDlet.GetFont( FONT_TEXT_BOLD ), "\n" + t + "\n\n\n\n\n", autoScroll, specialChars );
		if ( !autoScroll ) {
			text.setScrollFull( GameMIDlet.getScrollFull() );
			text.setScrollPage( GameMIDlet.getScrollPage() );
		}

		insertDrawable( text );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	/**
	 * 
	 * @param fonts
	 */
	public final void setFonts( ImageFont[] fonts ) {
		text.setFonts( fonts );
	}


	public final void keyPressed( int key ) {
		GameMIDlet.setMoveRight( false );
		text.keyPressed( key );
	}


	public final void keyReleased( int key ) {
		text.keyReleased( key );
	}


	public final void hideNotify( boolean deviceEvent ) {
		text.hideNotify( deviceEvent );
	}


	public final void showNotify( boolean deviceEvent ) {
		text.showNotify( deviceEvent );
	}

	
	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		text.setSize( width - MENU_LABEL_OFFSET_X, height );
		text.setPosition( MENU_LABEL_OFFSET_X, 0 );
	}


	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
			if ( text.contains( x, y ) )
				text.onPointerDragged( x, y );
		}


		public final void onPointerPressed( int x, int y ) {
			if ( text.contains( x, y ) )
				text.onPointerPressed( x, y );
//			else if ( labelBack.contains( x, y ) )
//				keyPressed( ScreenManager.KEY_BACK );
		}


		public final void onPointerReleased( int x, int y ) {
			if ( text.contains( x, y ) )
				text.onPointerReleased( x, y );
		}
	//#endif
	
}
