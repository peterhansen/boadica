/**
 * SplashEndemol.java
 *
 * Created on Aug 30, 2010 2:41:00 PM
 *
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.PaletteChanger;
import core.Constants;
import javax.microedition.lcdui.Image;

/**
 *
 * @author peter
 */
public final class SplashBoadica extends UpdatableGroup implements Constants, ScreenListener {

	private final DrawableImage logo;

	private final DrawableImage owl;

	private final Pattern bar;

	private final DrawableImage label;

	private static final short TIME_LOGO = 2300;

	private short accTime;


	public SplashBoadica() throws Exception {
		super( 8 );
		
		bar = new Pattern( new DrawableImage( PATH_SPLASH + "bar.png" ) );
		bar.setSize( Short.MAX_VALUE, bar.getHeight() );
		bar.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_LEFT );
		insertDrawable( bar );

		logo = new DrawableImage( PATH_SPLASH + "boadica.png" );
		logo.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
		insertDrawable( logo );

		owl = new DrawableImage( PATH_SPLASH + "coruja.png" );
		owl.defineReferencePixel( ANCHOR_LEFT | ANCHOR_BOTTOM );
		insertDrawable( owl );

		final Image fontImage = new PaletteChanger( PATH_SPLASH + "font_credits.png" ).createImage( 0xffffff, 0xffffff );
		final ImageFont font = ImageFont.createMultiSpacedFont( fontImage, PATH_SPLASH + "font_credits.bin" );
		font.setCharExtraOffset( 1 );

		label = new DrawableImage( PATH_SPLASH + "pesquisa.png" );
		label.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_LEFT );
		insertDrawable( label );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public final void update( int delta ) {
		super.update( delta );

		accTime += delta;
		//#if DEBUG == "true"
			accTime += 300;
		//#endif
		if ( accTime >= TIME_LOGO )
			GameMIDlet.setScreen( SCREEN_CATEGORIES );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		owl.setPosition( 0, ( height - label.getHeight() - owl.getHeight() ) >> 1 );
		bar.setPosition( 0, owl.getRefPixelY() - bar.getHeight() + SPLASH_OWL_BAR_OFFSET_Y );
		label.setRefPixelPosition( Math.min( width >> 1, width - label.getWidth() - 2 ), bar.getRefPixelY() );

		final int LOGO_Y = owl.getRefPixelY() + ( ( height - owl.getRefPixelY() ) >> 1 ) - ( logo.getHeight() >> 1 );

		if ( owl.getRefPixelY() <= height - logo.getHeight() )
			logo.setRefPixelPosition( width >> 1, LOGO_Y );
		else
			logo.setRefPixelPosition( Math.max( width >> 1, width - ( logo.getWidth() >> 1 ) - 2 ), LOGO_Y );
	}

}
