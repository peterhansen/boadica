/**
 * TitledPaneScreen.java
 *
 * Created on Feb 7, 2011 6:58:29 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.Form;
import core.Constants;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author peter
 */
public final class TitledPane extends UpdatableGroup implements Constants, KeyListener
 //#if TOUCH == "true"
	, PointerListener
//#endif
{

	private final Label title;

	private final Pattern line;

	private final Drawable pane;

	private final KeyListener keyListener;

	private final boolean paneComponent;

	private final boolean handleLeftRightKeys;

	private boolean hasSpecialKeyMapping;

	//#if TOUCH == "true"
		private final PointerListener pointerListener;
	//#endif

				
	public TitledPane( Drawable pane, String title, boolean handleLeftRightKeys ) throws Exception {
		super( 3 );

		this.handleLeftRightKeys = handleLeftRightKeys;

		this.title = new Label(
				//#if SCREEN_SIZE == "GIANT"
//# 					FONT_PANE
				//#else
				FONT_TEXT_BOLD
				//#endif
					, title );
		insertDrawable( this.title );

		line = new Pattern( 0x000000 );
		insertDrawable( line );

		this.pane = pane;
		paneComponent = pane instanceof Component;
		insertDrawable( pane );

		if ( pane instanceof KeyListener )
			keyListener = ( KeyListener ) pane;
		else
			keyListener = null;

		//#if TOUCH == "true"
			if ( pane instanceof PointerListener )
				pointerListener = ( PointerListener ) pane;
			else
				pointerListener = null;
		//#endif
	}


	public final boolean handleLeftRightKeys() {
		return handleLeftRightKeys;
	}


	public final void keyPressed( int key ) {
		if ( keyListener != null )
			keyListener.keyPressed( key );
	}


	public final void keyReleased( int key ) {
		if ( keyListener != null )
			keyListener.keyReleased( key );
	}


	public boolean hasSpecialKeyMapping() {
		return hasSpecialKeyMapping;
	}

	
	public void setHasSpecialKeyMapping(boolean hasSpecialKeyMapping) {
		this.hasSpecialKeyMapping = hasSpecialKeyMapping;
	}


	//#if TOUCH == "true"

		public final void onPointerDragged( int x, int y ) {
			if ( pointerListener != null ) {
				if ( paneComponent )
					pointerListener.onPointerDragged( x, y );
				else
					pointerListener.onPointerDragged( x - pane.getPosX(), y - pane.getPosY() );
			}
		}


		public final void onPointerPressed( int x, int y ) {
			if ( pointerListener != null ) {
				if ( paneComponent )
					pointerListener.onPointerPressed( x, y );
				else
					pointerListener.onPointerPressed( x - pane.getPosX(), y - pane.getPosY() );
			}
		}


		public final void onPointerReleased( int x, int y ) {
			if ( pointerListener != null ) {
				if ( paneComponent )
					pointerListener.onPointerReleased( x, y );
				else
					pointerListener.onPointerReleased( x - pane.getPosX(), y - pane.getPosY() );
			}
		}
		
	//#endif


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		final int WIDTH = width * 9 / 10;

		final int titleHeight = TITLED_PANE_TITLE_Y + title.getHeight();
		line.setSize( WIDTH, 1 );
		line.setPosition( ( width - WIDTH ) >> 1, titleHeight );
		title.setPosition( line.getPosX(), TITLED_PANE_TITLE_Y );

		pane.setPosition( line.getPosX(), titleHeight + 1 );
		
		if ( pane instanceof Form ) {
			// TODO 60 é um chute, mas aparentemente resolve o problema do touchkeypad - encontrar o número correto
			pane.setSize( WIDTH, height - titleHeight - ( getPosY() + PANE_HEIGHT_MAGICAL ) );
			//#if DEBUG == "true"
				System.out.println( "Titled Pane: " + title.getText() +" height - titleHeight : " + (height - titleHeight) +" | pos: "+ getPosition() );
			//#endif
			( ( Form ) pane ).refreshLayout();
		} else {
			pane.setSize( WIDTH, height - titleHeight - 1 );
		}
	}

	final void setTitle(String text) {
		this.title.setText(text);
	}
}
