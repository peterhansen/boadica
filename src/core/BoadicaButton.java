/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.MenuListener;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.DrawableBorder;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;

/**
 *
 * @author Caio
 */
public final class BoadicaButton extends Component implements Constants, KeyListener
	//#if TOUCH == "true"
		, PointerListener
	//#endif
{
	private Image img;
	private int[] data;
	private boolean changed;
	private int colorFactor;

	private final DrawableImage left;
	private final DrawableImage right;
	private final Pattern fill;

	private final Button button;

	private static DrawableImage LEFT;
	private static DrawableImage RIGHT;
	private static DrawableImage FILL;


	public BoadicaButton( int textID ) throws Exception {
		this( GameMIDlet.getText( textID ) );
	}


	public BoadicaButton( String text ) throws Exception {
		super( 5 );

		if ( LEFT == null ) {
			LEFT = new DrawableImage( PATH_MENU + "b_l.png" );
			RIGHT = new DrawableImage( PATH_MENU + "b_r.png" );
			FILL = new DrawableImage( PATH_MENU + "b_f.png" );
		}

		left = new DrawableImage( LEFT );
		right = new DrawableImage( RIGHT );
		fill = new Pattern( new DrawableImage( FILL ) );

		insertDrawable( left );
		insertDrawable( fill );
		insertDrawable( right );
		
		button = new Button( GameMIDlet.GetFont( FONT_PANE ), text );
		insertDrawable( button );

		changed = true;

		setSize( ( button.getWidth() * 12 / 10 ) + ( left.getWidth() + right.getWidth() ), 0 );
	}

	public void setText(String text){
		button.setText( text );
		changed = true;
	}


	public final void draw( Graphics g ) {
		try {
			if ( isVisible() ) {
			if( changed ) {
					renewSelectedImage( g );
				} else {
					g.drawRGB( data, 0, getWidth(), translate.x + getPosX(), translate.y + getPosY(), getWidth(),							   getHeight(), true );
				}
			}
		} catch ( Throwable th ) {
			//#if DEBUG == "true"
				AppMIDlet.log( th, "BoaDicaButton::draw" );
			//#endif
		}
	}


	public final void setSize( int width, int height ) {
		width = Math.max( width, BUTTON_MIN_WIDTH );
		height = Math.max( height, fill.getHeight() );
		
		super.setSize( width, height );

		fill.setSize( width - left.getWidth() - right.getWidth(), fill.getHeight() );
		fill.setPosition( left.getWidth(), 0 );
		right.setPosition( width - right.getWidth(), 0 );

		button.setPosition( ( width - button.getWidth() ) >> 1, ( getHeight() - button.getHeight() ) >> 1 );

		if ( img == null || getWidth() != img.getWidth() || getHeight() != img.getHeight() ) {
			final int DATA_LENGTH = getWidth() * getHeight();
			data = null;
			img = null;
			img = Image.createImage( getWidth(), getHeight() );
			data = new int[ DATA_LENGTH ];
			changed = true;
		}
	}


	public final void addEventListener( EventListener e ) {
		button.addEventListener( e );
	}


	public final void setFocus( boolean focus ) {
		super.setFocus( focus );
		button.setFocus( focus );
		refresh();
	}


	public final void setId( int id ) {
		button.setId( id );
	}
	
	
	public final void renewSelectedImage( Graphics g ) {
		if ( isVisible() ) {
			final Graphics g2 = img.getGraphics();
			g2.setColor( PANE_COLOR );
			g2.fillRect( 0, 0, getWidth(), getHeight() );
			final Point previousTranslate = new Point( translate );

			translate.set( -getPosX(), -getPosY() );
			super.draw( g2 );
			translate.set( previousTranslate );
			img.getRGB( data, 0, getWidth(), 0, 0, getWidth(), getHeight() );
			final int magenta = g.getDisplayColor(PANE_COLOR) & PANE_COLOR ;
			for ( int i = 0; i < data.length; ++i ) {
				final int color = g.getDisplayColor(data[i]);
				if ( ( color & PANE_COLOR ) == magenta ) {
					data[ i ] = 0;
				} else {
					data[ i ] = 0xff000000 | Drawable.changeColorLightness( color, colorFactor );
				}
			}
			g.drawRGB( data, 0, getWidth(), translate.x + getPosX(), translate.y + getPosY(), getWidth(), getHeight(), true );
			changed = false;
		}
	}


	private final void refresh() {
		final int previousColorFactor = colorFactor;

		switch ( button.getState() ) {
			case Button.STATE_PRESSED:
				colorFactor = -85;
			break;

			case Button.STATE_ROLLOVER:
				colorFactor = 35;
			break;

			default:
				colorFactor = 0;
			break;
		}

		changed = changed || ( colorFactor != previousColorFactor );
	}


	public final void keyPressed( int key ) {
		button.keyPressed( key );
		refresh();
	}


	public final void keyReleased( int key ) {
		button.keyReleased( key );
		refresh();
	}


	public final boolean contains( int x, int y ) {
		return ( y > getPosY() && y < getPosY() + getHeight() ) && ( x > getPosX() && x < getPosX() + getWidth() );
	}


	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
			if( contains( x, y ) )
				button.onPointerDragged( button.getPosX() + ( button.getWidth() >> 1 ), button.getPosY() + ( button.getHeight() >> 1 ) );
			else
				button.onPointerDragged( button.getPosX() - 8 , button.getPosY() - 8 );

			refresh();
		}


		public final void onPointerPressed( int x, int y ) {
			if( contains( x, y ) )
				button.onPointerPressed( button.getPosX() + ( button.getWidth() >> 1 ), button.getPosY() + ( button.getHeight() >> 1 ) );

			refresh();
		}


		public final void onPointerReleased( int x, int y ) {
			if( contains( x, y ) )
				button.onPointerReleased( button.getPosX() + ( button.getWidth() >> 1 ), button.getPosY() + ( button.getHeight() >> 1 ) );
			else
				button.onPointerReleased( button.getPosX() - 8 , button.getPosY() - 8 );

			refresh();
		}


	//#endif

	public final String getUIID() {
		return "";
	}


	public void setText( int strId ) {
		setText(GameMIDlet.getText( strId));
	}
}
