/**
 * ScrollBar.java
 * �2008 Nano Games.
 *
 * Created on Jul 16, 2008 1:16:34 PM.
 */

//#if JAR != "min"

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;


/**
 * 
 * @author Peter
 */
public final class ScrollBar extends DrawableGroup implements Constants {

	private static final byte TOTAL_ITEMS = 3;
	
	private static final byte IMG_TOP		= 0;
	private static final byte IMG_BOTTOM	= 1;
	private static final byte IMG_FILL		= 2;
	
	public static final byte TYPE_BACKGROUND = 0;
	public static final byte TYPE_FOREGROUND = 1;
	
	private final DrawableImage top;
	
	private final DrawableImage bottom;
	
	private final Pattern fill;
	
	private static DrawableImage[] IMAGES;

	private static final byte SCROLL_WIDTH = 14;
	
	
	public ScrollBar( byte type ) throws Exception {
		super( TOTAL_ITEMS );

		if ( IMAGES == null ) {
			IMAGES = new DrawableImage[ TOTAL_ITEMS ];

			for ( byte i = 0; i < TOTAL_ITEMS; ++i )
				IMAGES[ i ] = new DrawableImage( PATH_SCROLL + i + ".png" );
		}
		
		if ( type == TYPE_FOREGROUND ) {
			top = new DrawableImage( IMAGES[ IMG_TOP ] );
			insertDrawable( top );

			bottom = new DrawableImage( IMAGES[ IMG_BOTTOM ] );
			insertDrawable( bottom );

			fill = new Pattern( new DrawableImage( IMAGES[ IMG_FILL ] ) );
			fill.setSize( fill.getFill().getWidth(), 0 );
			fill.setPosition( 0, top.getHeight() );
			insertDrawable( fill );
		} else {
			top = null;
			bottom = null;
			
			fill = new Pattern( 0xffffff );
			fill.setSize( 2, 0 );
			fill.setPosition( ( SCROLL_WIDTH - fill.getWidth() ) >> 1, 0 );

			insertDrawable( fill );
		}
		
		size.x = SCROLL_WIDTH;
	}
	
	
	public final void setSize( int width, int height ) {
		if ( top == null ) {
			super.setSize( width, height );
			fill.setSize( fill.getWidth(), height );
		} else {
			super.setSize( width, Math.max( height, top.getHeight() + bottom.getHeight() ) );
			bottom.setPosition( 0, size.y - bottom.getHeight() );
			fill.setSize( fill.getWidth(), bottom.getPosY() - top.getHeight() );
		}
	}
	
}

//#endif