/**
 * TabbedPane.java
 *
 * Created on Feb 2, 2011 4:12:29 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import screens.GameMIDlet;
import screens.TitledPane;
import screens.WindowManager;

/**
 *
 * @author peter
 */
public final class TabbedPane extends UpdatableGroup implements Constants, KeyListener
//#if TOUCH == "true"
	, PointerListener
//#endif
{

	public static final byte STATE_NONE			= -1;
	public static final byte STATE_HIDDEN		= 0;
	public static final byte STATE_APPEARING	= 1;
	public static final byte STATE_VISIBLE		= 2;
	public static final byte STATE_HIDING		= 3;

	private byte state = STATE_NONE;

	private int accTime;

	private final BezierCurve tweener = new BezierCurve();

	/** Duração da transição (estados STATE_APPEARING e STATE_HIDING). */
	public static final short TRANSITION_TIME = 500;

	private Tab[] tabs = new Tab[ 0 ];

	private final DrawableGroup tabGroup;
	
	private TitledPane[] panes = new TitledPane[ 0 ];

	private short currentPane;

	private final Point biggestSize = new Point();

	private static final byte DRAG_NONE = 0;
	private static final byte DRAG_TAB	= 1;
	private static final byte DRAG_PANE	= 2;

	private byte dragMode;

	private final Pattern fill;
	private final Pattern lineGray;
	private final Pattern lineLight;

	private static final int COLOR_FILL			= 0xbcd8b8;
	private static final int COLOR_LINE_GRAY	= 0x888d87;
	private static final int COLOR_LINE_LIGHT	= 0xeaf3e9;

	private final WindowManager windowManager;

	private final int OFFSET_Y;

    private final int Y_MINIMUM_MARGIN;

	private int height;

    /**
     * Referência para a sombra de foco emitida pelo painel.
     */
    private Pattern focusShadow;

	//#if TOUCH == "true"
		private final Point posClick = new Point();
	//#endif


	public TabbedPane( WindowManager windowManager, Pattern focusShadow  ) throws Exception {
		super( 30 );

		this.windowManager = windowManager;

		OFFSET_Y = Math.max( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT ) >> 3;
        
        Y_MINIMUM_MARGIN = ScreenManager.SCREEN_HEIGHT < 160? 0: ScreenManager.SCREEN_HEIGHT >> 3;

		tabGroup = new DrawableGroup( 15 );
		tabGroup.setSize( 0, Short.MAX_VALUE );
		insertDrawable( tabGroup );

		fill = new Pattern( COLOR_FILL );
		tabGroup.insertDrawable( fill );

		lineGray = new Pattern( COLOR_LINE_GRAY );
		lineGray.setSize( Short.MAX_VALUE, 1 );
		tabGroup.insertDrawable( lineGray );

		lineLight = new Pattern( COLOR_LINE_LIGHT );
		lineLight.setSize( Short.MAX_VALUE, 1 );
		tabGroup.insertDrawable( lineLight );

        this.focusShadow = focusShadow;
	}


	public final void setPane( TitledPane pane, int iconIndex ) throws Exception {
		for ( byte i = 0; i < tabs.length; ++i ) {
			if ( tabs[ i ].getIconIndex() == iconIndex ) {
				removePane( i );
				addPane( pane, iconIndex, i, true );
				return;
			}
		}
	}


	public final void addPane( TitledPane pane, int iconIndex, int index, boolean refreshPanes ) throws Exception {
		final TitledPane[] newPanes = new TitledPane[ panes.length + 1 ];
		final Tab[] newTabs = new Tab[ tabs.length + 1 ];

		if ( index >= 0 ) {
			System.arraycopy( panes, 0, newPanes, 0, index );
			newPanes[ index ] = pane;
			System.arraycopy( panes, index, newPanes, index + 1, panes.length - index );

			System.arraycopy( tabs, 0, newTabs, 0, index );
			final Tab t = new Tab( iconIndex );
			newTabs[ index ] = t;
			System.arraycopy( tabs, index, newTabs, index + 1, tabs.length - index );

			insertDrawable( pane, index );
			tabGroup.insertDrawable( t, index );
		} else {
			System.arraycopy( panes, 0, newPanes, 0, panes.length );
			System.arraycopy( tabs, 0, newTabs, 0, tabs.length );

			newPanes[ panes.length ] = pane;
			final Tab t = new Tab( iconIndex );
			newTabs[ tabs.length ] = t;
			insertDrawable( pane );
			tabGroup.insertDrawable( t );
		}

		tabs = newTabs;
		panes = newPanes;

		setCurrentPane( currentPane );

		if ( refreshPanes )
			refreshPanes();
	}


	public final byte getState() {
		return state;
	}


	public final void removeAllPanes() {
		while ( panes.length > 0 ) {
			removePane( 0 );
		}
	}


	public final void removePane( Drawable pane ) {
		for ( short i = 0; i < panes.length; ++i ) {
			if ( panes[ i ] == pane ) {
				removePane( panes[ i ] );
				return;
			}
		}
	}


	public final void removePane( int index ) {
		if ( panes.length > 0 ) {
			removeDrawable( panes[ index ] );
			tabGroup.removeDrawable( tabs[ index ] );
			
			panes[ index ] = null;
			tabs[ index ] = null;

			for ( int i = index; i < panes.length - 1; ++i ) {
				panes[ i ] = panes[ i + 1 ];
				tabs[ i ] = tabs[ i + 1 ];
			}

			final TitledPane[] newPanes = new TitledPane[ panes.length - 1 ];
			final Tab[] newTabs = new Tab[ tabs.length - 1 ];
			System.arraycopy( panes, 0, newPanes, 0, newPanes.length );
			System.arraycopy( tabs, 0, newTabs, 0, newTabs.length );

			panes = newPanes;
			tabs = newTabs;
			setCurrentPane( currentPane );
		}
	}


	public final Tab getTabByIndex( int index ) {
		for ( byte i = 0; i < tabs.length; ++i ) {
			if ( tabs[ i ].getIconIndex() == index )
				return tabs[ i ];
		}

		return null;
	}


	public final void refreshPanes() {
		if ( tabs.length > 0 ) {

			int w = 0;
			for ( int i = 0; i < tabs.length; ++i ) {
				w += tabs[ i ].getWidth();
			}

			int spacing = TAB_MAX_SPACING;
			if(  tabs.length-1 > 0 ) {
				spacing = ( getWidth() - w ) / ( tabs.length+1 );
				if( spacing > TAB_MAX_SPACING ) spacing = TAB_MAX_SPACING;
			}

			int sideOff = spacing;

			fill.setPosition( 0, tabs[ 0 ].getHeight() - 2 );
			lineGray.setPosition( 0, fill.getPosY() );
			lineLight.setPosition( 0, lineGray.getPosY() + 1 );

			int x = sideOff;
			for ( int i = 0; i < tabs.length; ++i ) {
				tabs[ i ].setPosition( x, 0 );
				panes[ i ].setPosition( 0, tabs[ 0 ].getHeight() );
				panes[ i ].setSize( getWidth(), height - panes[ i ].getPosY() + magicFactor*PANE_HEIGHT_MAGICAL);
				x += tabs[ i ].getWidth() + spacing;
			}

//			if ( x > getWidth() ) {
//				tabGroup.setSize( x, tabGroup.getHeight() );
//			} else {
				tabGroup.setSize( getWidth(), tabGroup.getHeight() );
				final Drawable tabBack = tabs[ tabs.length - 1 ];
				tabBack.setPosition( getWidth() - tabBack.getWidth() - sideOff, 0 );
				tabGroup.setPosition( 0, 0 );
//			}
		}
	}


	private final TitledPane getCurrentPane() {
		if ( panes.length > 0 && currentPane < panes.length)
			return panes[ currentPane ];

		return null;
	}


	public final void update( int delta ) {
		switch ( state ) {
			case STATE_APPEARING:
				accTime += delta;
				refresh();

				if ( accTime >= TRANSITION_TIME ) {
					setState( STATE_VISIBLE );
				}
			break;

			case STATE_HIDING:
				accTime += delta;
				refresh();

				if ( accTime >= TRANSITION_TIME ) {
					setState( STATE_HIDDEN );
				}
			break;

			case STATE_VISIBLE:
				if ( getCurrentPane() instanceof Updatable )
					( ( Updatable ) getCurrentPane() ).update( delta );
			break;
		}
	}


	public final void previousPane() {
		setCurrentPane( currentPane - 1 );
	}

	
	public final void nextPane() {
		setCurrentPane( currentPane + 1 );
	}


	public final void setCurrentPane( int index ) {
		if ( panes.length > 0 ) {
			final Drawable previousPane = getCurrentPane();

			dragMode = DRAG_NONE;
			currentPane = ( short ) ( ( index + panes.length ) % panes.length );

			tabGroup.setDrawableIndex( fill, tabGroup.getUsedSlots() - 1 );
			tabGroup.setDrawableIndex( lineGray, tabGroup.getUsedSlots() - 1 );
			tabGroup.setDrawableIndex( lineLight, tabGroup.getUsedSlots() - 1 );

			final Drawable tab = tabs[ currentPane ];

			tabGroup.setDrawableIndex( tab, tabGroup.getUsedSlots() - 1 );
			
			if ( tab.getPosX() + tab.getWidth() + tabGroup.getPosX() > getWidth() ) {
				tabGroup.setPosition( getWidth() - ( tab.getPosX() + tab.getWidth() ), tabGroup.getPosY() );
				checkTabGroupLimits();
			} else if ( tab.getPosX() + tabGroup.getPosX() < 0 ) {
				tabGroup.setPosition( -tab.getPosX(), tabGroup.getPosY() );
				checkTabGroupLimits();
			}

			for ( byte i = 0; i < panes.length; ++i ) {
				panes[ i ].setVisible( i == currentPane );
				tabs[ i ].setActive( i == currentPane );
			}

			if ( getCurrentPane() != previousPane && ( previousPane instanceof KeyListener))
				((KeyListener)previousPane).keyReleased( 0 );

			GameMIDlet.setSpecialKeyMapping( getCurrentPane().hasSpecialKeyMapping() );
		}
	}


	public final void show() {
		switch ( state ) {
			case STATE_APPEARING:
			case STATE_VISIBLE:
			return;

			default:
				GameMIDlet.setSpecialKeyMapping( getCurrentPane().hasSpecialKeyMapping() );
				setState( STATE_APPEARING );
		}
	}


	public final void hide() {
		switch ( state ) {
			case STATE_HIDDEN:
			case STATE_HIDING:
			return;

			default:
				GameMIDlet.setSpecialKeyMapping( false );
				setState( STATE_HIDING );
		}
	}


	public final void setState( int state ) {
		switch ( state ) {
			case STATE_HIDDEN:
                focusShadow.setVisible( false );
				dragMode = DRAG_NONE;
				accTime = TRANSITION_TIME;
				setTween( 0, ScreenManager.SCREEN_HEIGHT - getPaneHeight() );
				refresh();
			break;

			case STATE_APPEARING:
                focusShadow.setVisible( true );
				switch ( this.state ) {
					case STATE_VISIBLE:
					return;
				}
				setTween( getPosY(), ScreenManager.SCREEN_HEIGHT - getHeight() );
				
				accTime = 0;
				refresh();
			break;

			case STATE_HIDING:
                focusShadow.setVisible( false );
				if ( this.state == STATE_HIDDEN )
					return;
				accTime = 0;
				setTween( getPosY(), ScreenManager.SCREEN_HEIGHT - getPaneHeight() );
				refresh();
			break;
		}

		this.state = ( byte ) state;
	}


	public final int getPanePosY() {
		return getPosY()+ getPaneHeight();
	}


	public final int getPaneHeight() {
		return tabs.length > 0 ? tabs[ 0 ].getHeight() : 0;
	}


	private final void setTween( int startY, int endY ) {
		tweener.origin.y = startY;

		if ( endY < startY ) {
			tweener.destiny.y = endY + OFFSET_Y;

			tweener.control1.y = startY + OFFSET_Y;
			tweener.control2.y = endY - OFFSET_Y;
		} else {
			tweener.destiny.y = endY;
			
			tweener.control1.y = startY - OFFSET_Y;
			tweener.control2.y = endY + OFFSET_Y;
		}

        // garante um percentual da tela livre
        if ( tweener.destiny.y < Y_MINIMUM_MARGIN ) {
            tweener.destiny.y = Y_MINIMUM_MARGIN;
        }
	}

    /**
     * Calcula tamanho da sombra de foco (trama) para que ela obscureça
     * o que estiver através das abas.
     */
    private void setShadowSize() {
        focusShadow.setSize(focusShadow.getWidth(), this.getPosY() + fill.getPosY() );
    }

	private final void refresh() {
		setRefPixelPosition( 0, tweener.getYFixed( NanoMath.divInt( accTime, TRANSITION_TIME ) ) );

        // usamos metade da altura da tela a fim de ter uma "gordurinha"
        // para o movimento do tween a fim de não faltar o background
        fill.setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
        setShadowSize();
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height + ( OFFSET_Y * 3 / 2 ) );

		this.height = height;
        setShadowSize();
		refreshPanes();
	}

	
	public final void keyPressed( int key ) {
		if ( ( getCurrentPane() instanceof TitledPane ) && !( ( TitledPane ) getCurrentPane() ).handleLeftRightKeys() ) {
			switch ( key ) {
				case ScreenManager.KEY_LEFT:
				case ScreenManager.KEY_NUM4:
					setCurrentPane( currentPane - 1 );
				break;

				case ScreenManager.KEY_RIGHT:
				case ScreenManager.KEY_NUM6:
					setCurrentPane( currentPane + 1 );
				break;

				default:
					if ( getCurrentPane() instanceof KeyListener ) {
						( ( KeyListener ) getCurrentPane() ).keyPressed( key );
					}
			}
		} else if ( getCurrentPane() instanceof KeyListener ) {
			( ( KeyListener ) getCurrentPane() ).keyPressed( key );
		}
	}


	public final void keyReleased( int key ) {
		if ( getCurrentPane() instanceof KeyListener ) {
			( ( KeyListener ) getCurrentPane() ).keyReleased( key );
		}
	}


	private final void checkTabGroupLimits() {
		tabGroup.setPosition( NanoMath.clamp( tabGroup.getPosX(), getWidth() - tabGroup.getWidth(), 0 ), 0 );
	}


//	public boolean contains( int x, int y ) {
//		if( super.contains( x, y ) ) {
//			for( int i = 0; i < panes.length; i++ ) {
//				if( panes[i].contains( x - getPosX(), y - getPosY() ) ) {
//					return true;
//				}
//			}
//		}
//		return false;
//	}


	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
			switch ( dragMode ) {
				case DRAG_TAB:
					tabGroup.move( x - posClick.x, 0 );
					checkTabGroupLimits();
					posClick.set( x, y );
				break;

				case DRAG_PANE:
					if ( getCurrentPane() instanceof PointerListener )
						( ( PointerListener ) getCurrentPane() ).onPointerDragged( x - panes[ currentPane ].getPosX(), y - panes[ currentPane ].getPosY() );
				break;
			}
		}


		public final void onPointerPressed( int x, int y ) {
			show();

			for ( byte i = 0; i < tabs.length; ++i ) {
				if ( tabs[ i ].contains( x, y ) ) {
					dragMode = DRAG_TAB;
					posClick.set( x, y );
					return;
				}
			}

			if ( getCurrentPane().contains( x, y ) && getCurrentPane() instanceof PointerListener ) {
				dragMode = DRAG_PANE;
				final PointerListener p = ( PointerListener ) getCurrentPane();
				p.onPointerPressed( x - getCurrentPane().getPosX(), y - getCurrentPane().getPosY() );
				return;
			}
		}


		public final void onPointerReleased( int x, int y ) {
			switch ( dragMode ) {
				case DRAG_TAB:
					final Point p = new Point( x, y );
					if ( p.distanceTo( posClick ) <= TOUCH_TOLERANCE ) {
						for ( byte i = 0; i < tabs.length; ++i ) {
							if ( tabs[ i ].contains( x - tabGroup.getPosX(), y ) ) {
								if ( i < tabs.length - 1 ) {
									setCurrentPane( i );
								} else if ( windowManager != null ) {
									windowManager.onBackPressed();
								}
							}
						}
					}
				break;

				case DRAG_PANE:
					if ( getCurrentPane() instanceof PointerListener )
						( ( PointerListener ) getCurrentPane() ).onPointerReleased( x - getCurrentPane().getPosX(), y - getCurrentPane().getPosY() );
				break;
			}

			dragMode = DRAG_NONE;
		}
	//#endif

}
