/**
 * MenuLabel.java
 * 
 * Created on Mar 2, 2009, 3:14:44 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Rectangle;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class PaneLabel extends UpdatableGroup implements Constants {

	private final MarqueeLabel label;

	private boolean current;

	private final Pattern barPattern;

	private static int PARENT_Y;

	private boolean selectable = true;

	private final DrawableImage icon;

	private static DrawableImage FILL;



	public PaneLabel( String text, int iconIndex ) throws Exception {
		super( 7 );

		if ( FILL == null )
			FILL =  new DrawableImage( PATH_MENU + "fill.png" );
		
		barPattern = new Pattern( new DrawableImage( FILL ) );
		barPattern.setSize( Short.MAX_VALUE, barPattern.getHeight() );
		barPattern.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( barPattern );

		icon = GameMIDlet.getIcon( iconIndex );
		icon.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( icon );

		label = new MarqueeLabel( FONT_PANE, text );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		insertDrawable( label );

		final Pattern line = new Pattern( 0x000000 );
		line.setSize( Short.MAX_VALUE, 1 );
		line.setPosition( 0, barPattern.getHeight() );
		insertDrawable( line );

		setSize( ScreenManager.SCREEN_WIDTH, barPattern.getHeight() + 1 );
			
		setText( text );
		setCurrent( false );
	}


	public final void setSelectable( boolean s ) {
		selectable = s;

		if ( !selectable ) {
			refreshLabel();
		}
	}


	public final void setCurrent( boolean c ) {
		if ( selectable ) {
			this.current = c;
			barPattern.setVisible( c );
			refreshLabel();
		}
	}


	private final void refreshLabel() {
		label.setViewport( new Rectangle( 0, 0, Short.MAX_VALUE, Short.MAX_VALUE ) );
		label.setScrollFrequency( ( current || !selectable ) ? MarqueeLabel.SCROLL_FREQ_IF_BIGGER : MarqueeLabel.SCROLL_FREQ_NONE );
		label.setTextOffset( 0 );
	}


	public final void setText( int textIndex ) {
		setText( GameMIDlet.getText( textIndex ) );
	}


	public final void setText( String text ) {
		label.setText( text, false );
		refreshLabel();
	}


	public static final void setParentY( int y ) {
		PARENT_Y = y;
	}


	public final void setSize( int width, int height ) {
		size.set( width, height );

		defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );

		final int ICON_WIDTH = width / 6;
		icon.setRefPixelPosition( ICON_WIDTH >> 1, height >> 1 );

		label.setSize( width - ICON_WIDTH, label.getHeight() );
		label.setPosition( ICON_WIDTH, ( height - label.getHeight() ) >> 1 );
	}


}
